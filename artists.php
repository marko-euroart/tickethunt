<?php get_header('header'); ?>

<div id="promo" class="subpage" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
		</div>

	</div>
</div>

<div id="main" class="subpage">
	<div class="container">

		<div class="subpage-title">
			<div class="current-showing">
				<h1>Madonna</h1>
				<p>50 Madonna shows found by TicketHunt</p>
			</div>
			<div class="event-actions">
				<ul>
					<li class="track">Track this event</li>
					<li class="location">Location
						<div class="results second-option">
							<ul>
								<li><a href="#">All locations</a></li>
								<li><a href="#">Near Beograd, Serbia</a></li>
								<li><a href="#">Custom location...</a></li>
							</ul>
						</div>
					</li>
					<li class="time">Time
						<div class="results third-option">
							<ul>
								<li><a href="#">Any day</a></li>
								<li><a href="#">This week</a></li>
								<li><a href="#">This month</a></li>
								<li><a href="#">Next month</a></li>
								<li class="break"></li>
								<li><a href="#">All times</a></li>
								<li><a href="#">Day</a></li>
								<li><a href="#">Night</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div id="artists-tabs">
			<div class="tabs">
				<ul class="buttons">
					<li class="btn two-rows selected"><a href="#tab-1">Search results <span>(50 events)</span></a></li>
					<li class="btn"><a href="#tab-2">Biography</a></li>
					<li class="btn"><a href="#tab-3">Tours</a></li>
				</ul>

				<div id="tab-1">
					<table class="results-table">
						<thead>
							<tr>
								<td>Date</td>
								<td>City</td>
								<td>State</td>
								<td>Venue</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="date">17/7/2014</td>
								<td>Dallas</td>
								<td>TX</td>
								<td>American Airlines Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Houston</td>
								<td>TX</td>
								<td>Toyota Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>San Antonio</td>
								<td>TX</td>
								<td>AT&T Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Chicago</td>
								<td>IL</td>
								<td>Air Canada Center</td>
							</tr>
							<tr>
								<td class="date">17/7/2014</td>
								<td>Dallas</td>
								<td>TX</td>
								<td>American Airlines Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Houston</td>
								<td>TX</td>
								<td>Toyota Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>San Antonio</td>
								<td>TX</td>
								<td>AT&T Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Chicago</td>
								<td>IL</td>
								<td>Air Canada Center</td>
							</tr>
							<tr>
								<td class="date">17/7/2014</td>
								<td>Dallas</td>
								<td>TX</td>
								<td>American Airlines Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Houston</td>
								<td>TX</td>
								<td>Toyota Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>San Antonio</td>
								<td>TX</td>
								<td>AT&T Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Chicago</td>
								<td>IL</td>
								<td>Air Canada Center</td>
							</tr>
							<tr>
								<td class="date">17/7/2014</td>
								<td>Dallas</td>
								<td>TX</td>
								<td>American Airlines Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Houston</td>
								<td>TX</td>
								<td>Toyota Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>San Antonio</td>
								<td>TX</td>
								<td>AT&T Center</td>
							</tr>
							<tr>
								<td class="date">16/7/2014</td>
								<td>Chicago</td>
								<td>IL</td>
								<td>Air Canada Center</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div id="tab-2">
					<h1>sdfsdfsdf</h1>
				</div>

				<div id="tab-3">
					<h1>ssdfsdfsdfsdfdf</h1>
				</div>

			</div>
		</div>

		<div class="recomended">
			<div class="title">Follow similar artists</div>
			<div class="box">
				<img src="<?php echo site_url() ?>img/small-thumb.jpg" alt="" class="thumb">
				<a href="#">U2</a>
				<div class="btns">
					<a href="#" class="favorite">favorite</a>
					<a href="#" class="close">close</a>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>