<!doctype html>
<!--[if IE 7 ]><html lang="hr" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="hr" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="hr" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Ticket hunt</title>	
	<link id="style" href="<?php echo site_url(); ?>css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/owl.transitions.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/fancySelect.css" rel="stylesheet" type="text/css">

	<link rel="shortcut icon" href="favicon.ico">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta property="og:site_name" content="">
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">

	<script type="text/javascript" src="//use.typekit.net/rhf4dex.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script defer src="<?php echo site_url(); ?>js/script.js"></script>
</head>
<body <?php body_class(); ?>>
<!-- Header -->
<header id="page-header" role="banner">
    <div class="container">
		<div class="site-branding">
			<h1 class="site-logo"><a href="<?php echo site_url(); ?>"  title="Site title">Ticket-hunt</a></h1>			
		</div>
		<div class="menu-btn"></div>

		<nav>
			<ul class="main-nav">
				<li class="selected">
					<a href="#">Concerts</a>
					<div class="sub-wrapper">
						<ul>
							<li><a href="#">Option 1</a></li>
							<li><a href="#">Option 2</a></li>
							<li><a href="#">Option 3</a></li>
							<li><a href="#">Option 4</a></li>
							<li><a href="#">Option 5</a></li>
						</ul>
						<ul>
							<li><a href="#">Option 6</a></li>
							<li><a href="#">Option 7</a></li>
							<li><a href="#">Option 8</a></li>
							<li><a href="#">Option 9</a></li>
							<li><a href="#">Option 10</a></li>
						</ul>
						<ul>
							<li><a href="#">Option 11</a></li>
							<li><a href="#">Option 12</a></li>
							<li><a href="#">Option 13</a></li>
							<li><a href="#">Option 14</a></li>
							<li class="more"><a href="#">More</a></li>
						</ul>
					</div>
				</li>
				<li>
					<a href="#">Sports</a>
					<ul class="categories">
						<li>
							<a href="#">Option 1</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
							</div>
						</li>
						<li>
							<a href="#">Option 2</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
								</ul>
							</div>
						</li>
						<li><a href="#">Option 3</a></li>
						<li><a href="#">Option 4</a></li>
						<li>
							<a href="#">Option 5</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
							</div>
						</li>
						<li><a href="#">Option 6</a></li>
						<li><a href="#">Option 7</a></li>
						<li>
							<a href="#">Option 8</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
								</ul>
							</div>
						</li>
						<li class="more"><a href="#">More</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Theatre</a>
					<div class="sub-wrapper">
						<ul>
							<li><a href="#">Option 1</a></li>
							<li><a href="#">Option 2</a></li>
							<li><a href="#">Option 3</a></li>
							<li><a href="#">Option 4</a></li>
							<li><a href="#">Option 5</a></li>
						</ul>
						<ul>
							<li><a href="#">Option 6</a></li>
							<li><a href="#">Option 7</a></li>
							<li><a href="#">Option 8</a></li>
							<li><a href="#">Option 9</a></li>
							<li><a href="#">Option 10</a></li>
						</ul>
						<ul>
							<li><a href="#">Option 11</a></li>
							<li><a href="#">Option 12</a></li>
							<li><a href="#">Option 13</a></li>
							<li><a href="#">Option 14</a></li>
							<li class="more"><a href="#">More</a></li>
						</ul>
					</div>
				</li>
				<li>
					<a href="#">Other</a>
					<ul class="categories">
						<li>
							<a href="#">Option 1</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
							</div>
						</li>
						<li>
							<a href="#">Option 2</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
								</ul>
							</div>
						</li>
						<li><a href="#">Option 3</a></li>
						<li><a href="#">Option 4</a></li>
						<li>
							<a href="#">Option 5</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
							</div>
						</li>
						<li><a href="#">Option 6</a></li>
						<li><a href="#">Option 7</a></li>
						<li>
							<a href="#">Option 8</a>
							<div class="sub-wrapper">
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
									<li><a href="#">Option 10</a></li>
									<li><a href="#">Option 11</a></li>
									<li><a href="#">Option 12</a></li>
									<li><a href="#">Option 13</a></li>
									<li><a href="#">Option 14</a></li>
									<li><a href="#">Option 15</a></li>
									
								</ul>
								<ul>
									<li><a href="#">Option 1</a></li>
									<li><a href="#">Option 2</a></li>
									<li><a href="#">Option 3</a></li>
									<li><a href="#">Option 4</a></li>
									<li><a href="#">Option 5</a></li>
									<li><a href="#">Option 6</a></li>
									<li><a href="#">Option 7</a></li>
									<li><a href="#">Option 8</a></li>
									<li><a href="#">Option 9</a></li>
								</ul>
							</div>
						</li>
						<li class="more"><a href="#">More</a></li>
					</ul>
				</li>
			</ul>
			<ul class="login-signup">
				<li class="login"><a href="#">Marko Petrinjac</a></li>
				<li class="signup"><a href="#">Sign up</a></li>
			</ul>	
			<ul class="main-nav-mob">
				<li><a href="#">Concerts</a></li>
				<li><a href="#">Sports</a></li>
				<li><a href="#">Theatre</a></li>
				<li><a href="#">Other</a></li>
			</ul>
		</nav>
    </div>

	<div id="modal" class="login-trigger">
		<div class="modal-container">
			<div class="modal-box">
				<div class="modal-content">
					<span class="close"></span>
					<form action="#">
						<h1>Login</h1>
						<div class="group">
							<input type="text" placeholder="Email">
						</div>
						<div class="group error">
							<input type="text" placeholder="Email">
						</div>
						<div class="group">
							<input type="password" placeholder="Password">
						</div>
						<div class="group error">
							<input type="password" placeholder="Password">
							<p class="error-msg">Error message goes here</p>
						</div>
						<div class="group">
							<input type="checkbox" name="checkbox" class="remember-me" id="remember">
							<label for="remember">Remember me</label>
						</div>
						<div class="group">
							<input type="submit" value="Sign in">
						</div>
					</form>
					<div class="socials-2">
						<h2>or</h2>
						<ul class="socials-list">
							<li class="face"><a href="#"></a></li>
							<li class="b"><a href="#"></a></li>
							<li class="ok"><a href="#"></a></li>
							<li class="twitter"><a href="#"></a></li>
							<li class="ggl"><a href="#"></a></li>
						</ul>
					</div>
					<div class="help">
						<a href="#">Forgot password</a>
						<p>No account? <a href="#">Sign Up!</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modal" class="registration-trigger">
		<div class="modal-container">
			<div class="modal-box">
				<div class="modal-content">
					<span class="close"></span>
					<form action="#">
						<h1>Please Register</h1>
						<p>... and find amazing tickets for great events!</p>
						<div class="group">
							<input class="user" type="text" placeholder="username">
						</div>
						<div class="group">
							<input class="email" type="text" placeholder="E-mail">
						</div>
						<div class="group">
							<input class="password" type="password" placeholder="Password">
						</div>
						<div class="group">
							<input class="password" type="password" placeholder="Repeat password">
						</div>
						<div class="group test-thumb">		
							<img src="<?php echo site_url() ?>img/test-sample.png" alt="*">
						</div>
						<div class="group">
							<input class="test" type="text" placeholder="Enter code below">
						</div>
						<div class="group">
							<input type="submit" value="Register">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</header>
<!-- /Header -->