<!doctype html>
<!--[if IE 7 ]><html lang="hr" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="hr" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="hr" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="hr" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Ticket hunt</title>	
	<link id="style" href="<?php echo site_url(); ?>css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/owl.transitions.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url(); ?>css/fancySelect.css" rel="stylesheet" type="text/css">
	<!-- <link href="<?php echo site_url(); ?>css/jquery-ui.css" rel="stylesheet" type="text/css"> -->
	<link href="<?php echo site_url(); ?>css/jquery.scrollbar.css" rel="stylesheet" type="text/css">
	
	<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"> -->

	<link rel="shortcut icon" href="favicon.ico">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="viewport" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta property="og:site_name" content="">
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">

	<script type="text/javascript" src="https://use.typekit.net/rhf4dex.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script defer src="<?php echo site_url(); ?>js/script.js"></script>
	<script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
</head>
<body <?php body_class(); ?>>
<!-- Header -->

<div class="mobile-nav" data-method="mobileNav">
	<p class="site-name">Ticket Hunt</p>

	<?php if( is_page('home') ): ?>
		<div class="profile-box-mobile login-mobile-box" data-method="mobileProfile">
			<div class="profile-inner login-inner">
				<p class="login-mobile"><a href="#">Login</a><span>|</span><a href="#">Sign up</a></p>
			</div>
		</div>
	<?php else : ?>
		<div class="profile-box-mobile" data-method="mobileProfile">
			<div class="profile-inner">
				<div class="img-wrapper">
					<img src="img/profile-1.jpg" alt>
				</div>
				<p class="profile-name">Welcome<br><span>Charlie</span></p>
			</div>
		</div>
		<div class="profile-menu-mob">
			<p class="back back-profile">Back</p>
			<ul class="menu-step-2">
				<li><a href="#">Account settings</a></li>
				<li><a href="#">Hunt</a></li>
				<li><a href="#">Social Networks</a></li>
				<li><a href="#">Logout</a></li>
			</ul>
		</div>
	<?php endif; ?>	

	<ul class="menu-step-1">
		<li class="step-1">
			<span>Concerts</span>
			<div class="mob-nav-2 menu-list">
				<p class="back back-1">Back</p>
				<p>Concerts</p>
				<ul class="menu-step-2">
					<li>
						<span>Concerts 1</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Concerts 1</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Concerts 2</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Concerts 2</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Concerts 3</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Concerts 3</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Concerts 4</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Concerts 4</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Concerts 5</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Concerts 5</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
		<li class="step-1">
			<span>Sports</span>
			<div class="mob-nav-2 menu-list">
				<p class="back back-1">Back</p>
				<p>Sports</p>
				<ul class="menu-step-2">
					<li>
						<span>Sports 1</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Sports 1</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Sports 2</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Sports 2</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Sports 3</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Sports 3</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Sports 4</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Sports 4</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Sports 5</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Sports 5</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
		<li class="step-1">
		<span>Theatre</span>
			<div class="mob-nav-2 menu-list">
				<p class="back back-1">Back</p>
				<p>Theatre</p>
				<ul class="menu-step-2">
					<li>
						<span>Theatre 1</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Theatre 1</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Theatre 2</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Theatre 2</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Theatre 3</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Theatre 3</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Theatre 4</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Theatre 4</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Theatre 5</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Theatre 5</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
		<li class="step-1">
			<span>Other</span>
			<div class="mob-nav-2 menu-list">
				<p class="back back-1">Back</p>
				<p>Other</p>
				<ul class="menu-step-2">
					<li>
						<span>Other 1</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Option 1</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Other 2</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Option 2</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Other 3</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Option 3</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Other 4</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Option 4</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
					<li>
						<span>Other 5</span>
						<div class="mob-nav-3 menu-list">
							<p class="back back-2">Back</p>
							<p>Option 5</p>
							<ul class="menu-step-3 menu-list">
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</div>

<div id="wrapper" class="cf">


	<header id="page-header" role="banner">
	    <div class="container">
			<div class="site-branding">
				<h1 class="site-logo"><a href="<?php echo site_url(); ?>"  title="Site title">Ticket-hunt</a></h1>			
			</div>
			<div class="menu-btn"></div>

			<nav>
				<ul class="main-nav">
					<li class="selected">
						<a href="#">Concerts</a>
						<div class="sub-wrapper">
							<ul>
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
							<ul>
								<li><a href="#">Option 6</a></li>
								<li><a href="#">Option 7</a></li>
								<li><a href="#">Option 8</a></li>
								<li><a href="#">Option 9</a></li>
								<li><a href="#">Option 10</a></li>
							</ul>
							<ul>
								<li><a href="#">Option 11</a></li>
								<li><a href="#">Option 12</a></li>
								<li><a href="#">Option 13</a></li>
								<li><a href="#">Option 14</a></li>
								<li class="more"><a href="#">More</a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="#">Sports</a>
						<ul class="categories">
							<li>
								<a href="#">Option 1</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
								</div>
							</li>
							<li>
								<a href="#">Option 2</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">Option 3</a></li>
							<li><a href="#">Option 4</a></li>
							<li>
								<a href="#">Option 5</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
								</div>
							</li>
							<li><a href="#">Option 6</a></li>
							<li><a href="#">Option 7</a></li>
							<li>
								<a href="#">Option 8</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
									</ul>
								</div>
							</li>
							<li class="more"><a href="#">More</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Theatre</a>
						<div class="sub-wrapper">
							<ul>
								<li><a href="#">Option 1</a></li>
								<li><a href="#">Option 2</a></li>
								<li><a href="#">Option 3</a></li>
								<li><a href="#">Option 4</a></li>
								<li><a href="#">Option 5</a></li>
							</ul>
							<ul>
								<li><a href="#">Option 6</a></li>
								<li><a href="#">Option 7</a></li>
								<li><a href="#">Option 8</a></li>
								<li><a href="#">Option 9</a></li>
								<li><a href="#">Option 10</a></li>
							</ul>
							<ul>
								<li><a href="#">Option 11</a></li>
								<li><a href="#">Option 12</a></li>
								<li><a href="#">Option 13</a></li>
								<li><a href="#">Option 14</a></li>
								<li class="more"><a href="#">More</a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="#">Other</a>
						<ul class="categories">
							<li>
								<a href="#">Option 1</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
								</div>
							</li>
							<li>
								<a href="#">Option 2</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">Option 3</a></li>
							<li><a href="#">Option 4</a></li>
							<li>
								<a href="#">Option 5</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
								</div>
							</li>
							<li><a href="#">Option 6</a></li>
							<li><a href="#">Option 7</a></li>
							<li>
								<a href="#">Option 8</a>
								<div class="sub-wrapper">
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
										<li><a href="#">Option 10</a></li>
										<li><a href="#">Option 11</a></li>
										<li><a href="#">Option 12</a></li>
										<li><a href="#">Option 13</a></li>
										<li><a href="#">Option 14</a></li>
										<li><a href="#">Option 15</a></li>
										
									</ul>
									<ul>
										<li><a href="#">Option 1</a></li>
										<li><a href="#">Option 2</a></li>
										<li><a href="#">Option 3</a></li>
										<li><a href="#">Option 4</a></li>
										<li><a href="#">Option 5</a></li>
										<li><a href="#">Option 6</a></li>
										<li><a href="#">Option 7</a></li>
										<li><a href="#">Option 8</a></li>
										<li><a href="#">Option 9</a></li>
									</ul>
								</div>
							</li>
							<li class="more"><a href="#">More</a></li>
						</ul>
					</li>
				</ul>

				<?php if( is_page('home') ): ?>
					<ul class="login-signup">
						<li class="login"><a href="#">Login</a></li>
						<li class="signup"><a href="#">Sign up</a></li>
					</ul>
				<?php else : ?>
					<div class="profile-box" data-method="profileMenu">
						<div class="profile-inner">
							<div class="img-wrapper">
								<img src="img/profile-1.jpg" alt>
							</div>
							<p class="profile-name">Welcome <span>Charlie</span></p>
						</div>
						<div class="profile-menu">
							<ul>
								<li><a href="#">Account settings</a></li>
								<li><a href="#">Hunt</a></li>
								<li><a href="#">Social Networks</a></li>
								<li><a href="#">Logout</a></li>
							</ul>
						</div>
					</div>
				<?php endif; ?>	

				<ul class="main-nav-mob">
					<li><a href="#">Concerts</a></li>
					<li><a href="#">Sports</a></li>
					<li><a href="#">Theatre</a></li>
					<li><a href="#">Other</a></li>
				</ul>
			</nav>
	    </div>

		<div id="modal" class="login-trigger">
			<div class="modal-container">
				<div class="modal-box">
					<div class="modal-content">
						<span class="close"></span>
						<form action="#">
							<h1>Login</h1>
							<div class="group">
								<input type="text" placeholder="Email">
							</div>
							<div class="group error">
								<input type="text" placeholder="Email">
							</div>
							<div class="group">
								<input type="password" placeholder="Password">
							</div>
							<div class="group error">
								<input type="password" placeholder="Password">
								<p class="error-msg">Error message goes here</p>
							</div>
							<div class="group">
								<input type="checkbox" name="checkbox" class="remember-me" id="remember">
								<label for="remember">Remember me</label>
							</div>
							<div class="group">
								<input type="submit" value="Sign in">
							</div>
						</form>
						<div class="socials-2">
							<h2>or</h2>
							<ul class="socials-list">
								<li class="face"><a href="#"></a></li>
								<li class="b"><a href="#"></a></li>
								<li class="ok"><a href="#"></a></li>
								<li class="twitter"><a href="#"></a></li>
								<li class="ggl"><a href="#"></a></li>
							</ul>
						</div>
						<div class="help">
							<a href="#" class="forgot-pass">Forgot password</a>
							<p>No account? <a href="#">Sign Up!</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="modal" class="registration-trigger">
			<div class="modal-container">
				<div class="modal-box">
					<div class="modal-content">
						<span class="close"></span>
						<form action="#">
							<h1>Please Register</h1>
							<p>... and find amazing tickets for great events!</p>
							<div class="group">
								<input class="user" type="text" placeholder="Username">
							</div>
							<div class="group">
								<input class="email" type="text" placeholder="E-mail">
							</div>
							<div class="group">
								<input class="password" type="password" placeholder="Password">
							</div>
							<div class="group">
								<input class="password" type="password" placeholder="Repeat password">
							</div>
							<div class="group test-thumb">		
								<img src="<?php echo site_url() ?>img/test-sample.png" alt="*">
							</div>
							<div class="group">
								<input class="test" type="text" placeholder="Enter code below">
							</div>
							<div class="group">
								<input type="submit" value="Register">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div id="modal" class="change-pass-trigger">
			<div class="modal-container">
				<div class="modal-box">
					<div class="modal-content">
						<span class="close"></span>
						<form action="#">
							<h1>Change password</h1>
							<div class="group">
								<input class="password" type="password" placeholder="Old password">
							</div>
							<div class="group">
								<input class="password" type="password" placeholder="New password">
							</div>
							<div class="group">
								<input class="password" type="password" placeholder="Repeat password">
							</div>
							<div class="group">
								<input type="submit" value="Save">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div id="modal" class="restore-pass-trigger">
			<div class="modal-container">
				<div class="modal-box">
					<div class="modal-content">
						<span class="close-restore"></span>
						<form action="#">
							<h1>Restore password</h1>
							<p>Please enter the email address you used to register with and we’ll send you a link to reset your password.</p>
							<div class="group">
								<input class="email" type="text" placeholder="E-mail">
							</div>
							<div class="group test-thumb">		
								<img src="<?php echo site_url() ?>img/test-sample.png" alt="*">
							</div>
							<div class="group">
								<input class="test" type="text" placeholder="Enter code">
							</div>
							<div class="group">
								<input type="submit" value="Send">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</header>
	<!-- /Header -->