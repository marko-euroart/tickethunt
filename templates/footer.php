	<footer id="page-footer" role="contentinfo">
		<div class="container">

			<h1 class="logo"><a href="#">Ticket hunt</a></h1>

			<ul class="footer-nav">
				<li><a href="#">About us</a></li>
				<li><a href="#">Press</a></li>
				<li><a href="#">Jobs</a></li>
				<li><a href="#">Privacy policy</a></li>
				<li><a href="#">Terms of use</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>

			<div class="social-wrapper">
				<ul class="socials">
					<li><a href="#">
						<div class="flip">
							<div class="inner">
								<div class="sticker front">
									<img src="<?php echo site_url() ?>img/face-icon.png" alt="*">
								</div>
								<div class="sticker back">
									<img src="<?php echo site_url() ?>img/face-icon.png" alt="*">
								</div>
							</div>
						</div>
					</a></li>
					<li><a href="#">
						<div class="flip">
							<div class="inner">
								<div class="sticker front">
									<img src="<?php echo site_url() ?>img/twitter-icon.png" alt="*">
								</div>
								<div class="sticker back">
									<img src="<?php echo site_url() ?>img/twitter-icon.png" alt="*">
								</div>
							</div>
						</div>
					</a></li>
					<li><a href="#">
						<div class="flip">
							<div class="inner">
								<div class="sticker front">
									<img src="<?php echo site_url() ?>img/rss-icon.png" alt="*">
								</div>
								<div class="sticker back">
									<img src="<?php echo site_url() ?>img/rss-icon.png" alt="*">
								</div>
							</div>
						</div>
					</a></li>
					<li><a href="#">
						<div class="flip">
							<div class="inner">
								<div class="sticker front">
									<img src="<?php echo site_url() ?>img/envelope-icon.png" alt="*">
								</div>
								<div class="sticker back">
									<img src="<?php echo site_url() ?>img/envelope-icon.png" alt="*">
								</div>
							</div>
						</div>
					</a></li>
				</ul>
			</div>

			<div class="disclaimer">
				<p>All rights reserved 2014 (C)</p>
			</div>
	    
		</div>
	</footer>

</div><!-- /Wrapper -->
</body>
</html>