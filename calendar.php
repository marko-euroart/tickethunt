<?php get_header('header'); ?>

<div id="promo" class="subpage" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
		</div>

	</div>
</div>

<div id="main" class="subpage">
	<div class="container">

		<div class="search-wrapper">
			<form action="#" class="search-form">
				<input type="text" class="search-bar" placeholder="Search by name, sport, venue..." />
				<input type="submit" class="search-btn" value="search">
			</form>
			<div class="contextual-search">
				<a href="#">Show all results...</a>
				<h1 class="title top-results">Top result</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title performers">Performers</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Nets</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Winter Festival</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Holiday International</h1>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title events">Events</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Celebrate Brooklyn - Perf. Arts Fest with Nick Cave & The Bad Seeds,
									Devendra Banhart, Nicole Atkins
								</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title venues">Venues</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Academy of Music</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>

		


	</div>


<div class="calendar-container">

	<div id="calendar-top" class="calendar-top">
		<div id="events" data-method="eventSearch stickyCalendar">
			<div class="events-inner">
				<h1 class="title">Events near Belgrade, Serbia<span></span></h1>
				<div class="search-event">
					<p>Find events outside <span>Belgrade, Serbia:</span></p>
					<input type="search" placeholder="Start typing a city or zip code...">
				</div>
			</div>
		</div>

		<div class="calendar-icons cf" data-method="calendarToppopups">
			<ul>
				<li class="ticket-1">
					<div class="inner">
						<i class="cal-icon"></i>
						<p>On sale</p>
						<p><span>3</span>/15</p>
					</div>
					<div class="icons-popup onsale">
						<h1>Your on-sale notifications</h1>
						<i class="close-popup"></i>
						<ul>
							<li class="active">
								<p class="event-name">Tegan and Sara <span>12.08.2014.</span></p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list pink"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara 12.08.2014.</p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list pink"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara 12.08.2014.</p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list pink"></i>
							</li>
						</ul>
						<button class="popup-btn pink">All notifications</button>
					</div>
				</li>
				<li class="ticket-2">
					<div class="inner">
						<i class="cal-icon"></i>
						<p>On buy<br></p>
						<p>54</p>
					</div>
					<div class="icons-popup onbuy">
						<h1>Your on-buy notifications</h1>
						<i class="close-popup"></i>
						<ul>
							<li class="active">
								<p class="event-name">Tegan and Sara <span>12.08.2014.</span></p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list blue"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara 12.08.2014.</p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list blue"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara 12.08.2014.</p>
								<p class="event-info">The ticket you have on sale has been <span>requested</span></p>
								<i class="close-list blue"></i>
							</li>
						</ul>
						<button class="popup-btn pink">All notifications</button>
					</div>
				</li>
				<li class="favorite">
					<div class="inner">
						<i class="cal-icon"></i>
						<p>Following<br></p>
						<p><span>3</span>/15</p>
					</div>
					<div class="icons-popup following-popup">
						<h1>Following</h1>
						<i class="close-popup"></i>
						<ul>
							<li class="active">
								<p class="event-name">Tegan and Sara</p>
								<p class="event-info">The ticket you have on sale has been</p>
								<i class="close-list pink"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara</p>
								<p class="event-info">The ticket you have on sale has been</p>
								<i class="close-list pink"></i>
							</li>
							<li class="active">
								<p class="event-name">Tegan and Sara</p>
								<p class="event-info">The ticket you have on sale has been</p>
								<i class="close-list pink"></i>
							</li>
						</ul>
						<button class="popup-btn pink">All notifications</button>
					</div>
				</li>
				<li class="watch">
					<div class="inner">
						<i class="cal-icon"></i>
						<p>Social<br></p>
						<p><span>356</span>/111000</p>
					</div>
					<div class="icons-popup social-popup">
						<h1>Social notifications</h1>
						<i class="close-popup"></i>
						<ul>
							<li class="active">
								<p class="friends-number">245</p>
								<p class="friends-info">of your friends shared, liked &amp; attended <span>Bryan Adams</span></p>
								<div class="friends-inner">
									<ul class="friends-list">
										<li>
											<img src="img/friend-1.jpg" alt>
										</li>
										<li>
											<img src="img/friend-2.jpg" alt>
										</li>
										<li>
											<img src="img/friend-3.jpg" alt>
										</li>
										<li>
											<img src="img/friend-4.jpg" alt>
										</li>
										<li>
											<img src="img/friend-1.jpg" alt>
										</li>
										<li>
											<img src="img/friend-2.jpg" alt>
										</li>
									</ul>
									<i class="close-list pink"></i>
									<a href="#" class="friends-more"></a>
								</div>
							</li>
							<li class="active">
								<p class="friends-number">3</p>
								<p class="friends-info">of your friends shared, liked &amp; attended <span>Nine Inch Nails</span></p>
								<div class="friends-inner">
									<ul class="friends-list">
										
										<li>
											<img src="img/friend-4.jpg" alt>
										</li>
										<li>
											<img src="img/friend-2.jpg" alt>
										</li>
										<li>
											<img src="img/friend-3.jpg" alt>
										</li>
									</ul>
									<i class="close-list pink"></i>
								</div>
							</li>
							<li class="active">
								<p class="friends-number">291</p>
								<p class="friends-info">of your friends shared, liked &amp; attended <span>King of Leon</span></p>
								<div class="friends-inner">
									<ul class="friends-list">
										<li>
											<img src="img/friend-1.jpg" alt>
										</li>
										<li>
											<img src="img/friend-2.jpg" alt>
										</li>
										<li>
											<img src="img/friend-3.jpg" alt>
										</li>
										<li>
											<img src="img/friend-4.jpg" alt>
										</li>
										<li>
											<img src="img/friend-1.jpg" alt>
										</li>
										<li>
											<img src="img/friend-2.jpg" alt>
										</li>
									</ul>
									<i class="close-list pink"></i>
									<a href="#" class="friends-more"></a>
								</div>
							</li>
						</ul>
						<button class="popup-btn pink">All notifications</button>
					</div>
				</li>
			</ul>
		</div>

		<div class="days-top">
			<p class="month-top">October</p>
			<ul>
				<li>Monday</li>
				<li>Tuesday</li>
				<li>Wednesday</li>
				<li>Thursday</li>
				<li>Friday</li>
				<li>Saturday</li>
				<li>Sunday</li>
			</ul>
		</div>
	</div>

	<div class="calendar-wrapper" data-method="calendarSlider">
		<div class="calendar-inner" data-method="calendarPopups calendarAnimations">

			<table>
				<thead>
					<tr>
						<th>Monday</th>
						<th>Tuesday</th>
						<th>Wednesday</th>
						<th>Thursday</th>
						<th>Friday</th>
						<th class="weekend-day">Saturday</th>
						<th class="weekend-day">Sunday</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="mon today">
							<div class="inner-box">
								<div class="day-header">
									<p class="day">Wednesday</p>
									<p class="date">1</p>
									<p class="today-mark">Today</p>
								</div>
								<div class="day-content">
									<button class="top-btn"></button>
									<button class="down-btn"></button>
									<div class="popups-wrapper">
										<div class="ticket-box sale calendar-popup box-sale-1">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>

										<div class="ticket-box sale calendar-popup box-sale-2">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>

										<div class="ticket-box sale calendar-popup box-sale-3">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>

										<div class="ticket-box sale calendar-popup box-sale-4">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>

										<div class="ticket-box sale calendar-popup box-sale-5">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</div>
									<ul class="newsticker">
										<li class="ticket-sale" data-id="sale-1"><span>Tegan and Sara </span><span class="pink" class="pink">$431</span>
											
										</li>
										<li class="ticket-sale" data-id="sale-2"><span>Napalm Death</span>
											
										</li>
										<li class="ticket-sale" data-id="sale-3"><span>Napalm Death</span>
											
										</li>
										<li class="ticket-sale" data-id="sale-4"><span>Napalm Death</span>
											
										</li>
										<li class="ticket-sale" data-id="sale-5"><span>Napalm Death</span>
											
										</li>
									</ul>
								</div>
							</div>
						</td>
						<td class="tue">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">2</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span>Tegan and Sara </span><span class="pink">$431</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
								</ul>
								
							</div>
							</div>
						</td>
						<td class="wed">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">3</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span><span>Tegan and Sara </span><span class="pink">$431</span>
										<div class="ticket-box onsale calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="ticket-onsale">The ticket you have on sale has been <span>requested</span></p>
												<button class="popup-btn notifi-btn">All notifications</button>
											</div>
										</div>
									</li>
									<li class="ticket-buy"><span class="tick-buy"></span><span>Napalm Death</span>
										<div class="ticket-box onbuy calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="ticket-onbuy">The ticket you have on sale has been <span>requested</span></p>
												<button class="popup-btn notifi-btn">All notifications</button>
											</div>
										</div>
									</li>

									<li class="favorite-event"><span class="social-fav"></span><span>His Electro Blue Voice</span>
										<div class="favorite-box calendar-popup">
											<div class="favorite-img">
												<i class="close-box"></i>
												<img src="img/cal-img-6.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="info">Artist you are following has a show</p>
												<p class="artist">Beyonce with Jay-Z</p>
												<p class="place">Rose Bowl, Pasadena, on 2Aug 2014 at 20:00</p>
												<a href="#">Event page</a>
											</div>
										</div>
									</li>
									<li class="favorite-event"><span class="social-fav"></span><span>His Electro Blue Voice</span>
										<div class="favorite-box calendar-popup">
											<div class="favorite-img">
												<i class="close-box"></i>
												<img src="img/cal-img-6.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="info">Artist you are following has a show</p>
												<p class="artist">Beyonce with Jay-Z</p>
												<p class="place">Rose Bowl, Pasadena, on 2Aug 2014 at 20:00</p>
												<a href="#">Event page</a>
											</div>
										</div>
									</li>
									<li class="favorite-event"><span class="social-fav"></span><span>His Electro Blue Voice</span>
										<div class="favorite-box calendar-popup">
											<div class="favorite-img">
												<i class="close-box"></i>
												<img src="img/cal-img-6.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="info">Artist you are following has a show</p>
												<p class="artist">Beyonce with Jay-Z</p>
												<p class="place">Rose Bowl, Pasadena, on 2Aug 2014 at 20:00</p>
												<a href="#">Event page</a>
											</div>
										</div>
									</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p class="friends-popup-trigger">Tegan and Sara</p>

										<!-- Friends popup -->
										<div class="friends-box calendar-popup">
											<i class="close-box close-2"></i>
											<ul>
												<li>
													<p><span>245</span> of your friends liked this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>43</span> of your friends shared this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>3</span> of your friends attended this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div><!-- /Friends popup -->
										
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="thu">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">4</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span>Tegan and Sara</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
									<li class="ticket-sale"><span>Napalm Death</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
									<li class="ticket-sale"><span>His Electro Blue Voice</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="fri">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">5</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span><span>King of Leon </span><span class="pink">$431</span>
										<div class="ticket-box onsale calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="ticket-onsale">The ticket you have on sale has been <span>requested</span></p>
												<button class="popup-btn notifi-btn">All notifications</button>
											</div>
										</div>
									</li>
									<li class="ticket-buy"><span class="tick-buy"></span><span>Rod Stewart</span>
										<div class="ticket-box onbuy calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="ticket-onbuy">The ticket you have on sale has been <span>requested</span></p>
												<button class="popup-btn notifi-btn">All notifications</button>
											</div>
										</div>
									</li>
									<li class="favorite-event"><span class="social-fav"></span><span>30 Seconds to Mars</span>
										<div class="favorite-box calendar-popup">
											<div class="favorite-img">
												<i class="close-box"></i>
												<img src="img/cal-img-6.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="info">Artist you are following has a show</p>
												<p class="artist">Beyonce with Jay-Z</p>
												<p class="place">Rose Bowl, Pasadena, on 2Aug 2014 at 20:00</p>
												<a href="#">Event page</a>
											</div>
										</div>
									</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-3.jpg" alt>
									<div class="img-inner">
										<p class="friends-popup-trigger">Kings of Leon</p>

										<!-- Friends popup -->
										<div class="friends-box calendar-popup">
											<i class="close-box close-2"></i>
											<ul>
												<li>
													<p><span>245</span> of your friends liked this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>43</span> of your friends shared this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>3</span> of your friends attended this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div><!-- /Friends popup -->
										
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="sat">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">6</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span>Simlan Mobile Disco </span><span class="pink">$431</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
									<li class="ticket-buy"><span>Camo &amp; Krooked</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
									<li class="favorite-event"></span><span>Sinead O'Connor</span>
										<div class="favorite-box calendar-popup">
											<div class="favorite-img">
												<i class="close-box"></i>
												<img src="img/cal-img-6.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="info">Artist you are following has a show</p>
												<p class="artist">Beyonce with Jay-Z</p>
												<p class="place">Rose Bowl, Pasadena, on 2Aug 2014 at 20:00</p>
												<a href="#">Event page</a>
											</div>
										</div>
									</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-5.jpg" alt>
									<div class="img-inner">
										<p class="friends-popup-trigger">Camo &amp; Krooked</p>

										<!-- Friends popup -->
										<div class="friends-box calendar-popup">
											<i class="close-box close-2"></i>
											<ul>
												<li>
													<p><span>245</span> of your friends liked this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>43</span> of your friends shared this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-1.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
														</ul>
														<a href="#" class="friends-more"></a>
													</div>
												</li>
												<li>
													<p><span>3</span> of your friends attended this event</p>
													<div class="friends-inner">
														<ul class="friends-list">
															<li>
																<img src="img/friend-4.jpg" alt>
															</li>
															<li>
																<img src="img/friend-2.jpg" alt>
															</li>
															<li>
																<img src="img/friend-3.jpg" alt>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div><!-- /Friends popup -->

									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="sun">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">7</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span>Kings of Leon</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
									<li class="ticket-sale"><span>Napalm Death</span>
										<div class="ticket-box calendar-popup">
											<div class="ticket-img">
												<i class="close-box"></i>
												<img src="img/cal-img-1-big.jpg" alt>
											</div>
											<div class="inner cf">
												<p class="artist">Tegan and Sara</p>
												<p class="date">Monday, June 23, 6.30pm</p>
												<p class="place">Levitt pavillion SteelSacks Bethlehem, PA</p>
												<p class="price">From: <span>$89</span></p>
												<button class="popup-btn follow-btn">Follow</button>
												<button class="popup-btn ticket-btn">Buy</button>
											</div>
										</div>
									</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="mon">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Wednesday</p>
								<p class="date">8</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-4.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="tue">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">9</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
						<!-- 		<div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="wed">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">10</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="thu">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">11</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="fri">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">12</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-3.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="sat">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">13</p>
							</div>
							<div class="no-events-box">
								<p>No events</p>
							</div>
							</div>
						</td>
						<td class="sun">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">14</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="mon">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Wednesday</p>
								<p class="date">15</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="tue">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">16</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="wed">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">17</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="thu">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">18</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="fri">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">19</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sat">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">20</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sun">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">21</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="mon">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Wednesday</p>
								<p class="date">22</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="tue">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">23</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="wed">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">24</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="thu">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">25</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="fri">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">26</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sat">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">27</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sun">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">28</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="mon">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Wednesday</p>
								<p class="date">29</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="tue">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">30</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="wed">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">31</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="thu new-month">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">1</p>
								<p class="month">October</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
						<td class="fri">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">2</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sat">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">3</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<!-- <div class="day-img-box">
									<img class="img-box" src="img/cal-img-1.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div> -->
							</div>
							</div>
						</td>
						<td class="sun">
							<div class="inner-box">
							<div class="day-header">
								<p class="day">Thursday</p>
								<p class="date">4</p>
							</div>
							<div class="day-content">
								<button class="top-btn"></button>
								<button class="down-btn"></button>
								<ul class="newsticker">
									<li class="ticket-sale"><span class="tick-sale"></span>Tegan and Sara <span class="pink">$431</span></li>
									<li class="ticket-buy"><span class="tick-buy"></span>Napalm Death</li>
									<li class="favorite-event"><span class="social-fav"></span>His Electro Blue Voice</li>
								</ul>
								<div class="day-img-box">
									<img class="img-box" src="img/cal-img-2.jpg" alt>
									<div class="img-inner">
										<p>Tegan and Sara</p>
									</div>
								</div>
							</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</div>












</div>

<?php get_footer(); ?>