<?php
/*** Simple Rouing file ***/
// In general, readability is more important than cleverness or brevity.

// Define folder name
if( !defined( 'FOLDER_NAME' ) )		
	define( 'FOLDER_NAME', basename(__DIR__) );

// Define root path
if( !defined('ABSPATH') )
	define('ABSPATH', pathinfo(__FILE__, PATHINFO_DIRNAME ) . '/');

// Project stuff 
$site['enable_export_to_html'] = true;

// Include functions
require( ABSPATH . 'system/functions.php' );
// add_url_trailing_slash();

// Check if we have our homepage
if( !file_exists( ABSPATH . 'home.php' ) ) {
	echo 'We need home.php file!';
	die();
}

// Start Routing
$uri_segments = get_uri_segments();
$file_name = strtok(array_pop( $uri_segments ), '?' );


if( $file_name == 'home.php' || $file_name == FOLDER_NAME || $file_name == '' ) {
	get_file( 'home' );
} else {
	if( file_exists( ABSPATH . $file_name . '.php' ) ) {
		get_file( $file_name );
	} else {
		show_404_page();
	}
}

