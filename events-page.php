<?php get_header('header'); ?>

<div id="promo" class="subpage" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
		</div>

	</div>
</div>

<div id="main" class="subpage">
	<div class="container">

		<div class="search-wrapper event-search">
			<div class="search-result">
				<p>Your search</p>
				<p class="event-result">Beyonce with Jay-Z</p>
				<p>Sun Life Stadium - Miami Gardens, FL on Wed Jun 25 at 8:00pm</p>
			</div>
			<div class="event-actions">
				<ul>
					<li class="track">Track this event</li>
					<li class="share">Share</li>
					<li class="tickets">My tickets</li>
				</ul>
			</div>
		</div>

		<div class="events-wrapper">
			<ul class="event-tabs" data-method="eventsTabs">
				<li class="buy-trigger active" data-tab="1">Buy</li>
				<li class="sell-trigger" data-tab="2">Sell</li>
			</ul>

			<div class="events-content" data-method="eventsDetail">

				<!-- Tab buy -->
				<div class="tab-buy">
					<h1 class="title">Check out these great deals</h1>

					<!-- Buy list -->
					<div class="buy-list">
						<div class="group select-group">
							<div class="select-inner first-select disabled">
								<p>Sort by:</p>
								<select class="select-1" disabled>
									<option>Deal Score</option>
									<option>Option 1</option>
									<option>Option 2</option>
								</select>
								<p class="soon">Coming soon</p>
							</div>
							<div class="select-inner second-select">
								<p>Number of tickets:</p>
								<select class="select-1">
									<option>All</option>
									<option>Option 1</option>
									<option>Option 2</option>
								</select>
							</div>
						</div>
						<div class="price-filter">
							<p>Price filter</p>
							<p id="min-amount"><span>50</span>$</p>
							<p id="max-amount"><span>400</span>$</p>
							<div id="slider-range" data-method="uiSlider">
							</div>
						</div>

						<div class="events-list">
							<div class="scrollbar-outer">
								<ul>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
									<li>
										<p class="event-nr">75</p>
										<div class="ticket-info">
											<p class="section-nr">Section 431</p>
											<p class="ticket-available">8 tickets available</p>
										</div>
										<div class="img-box">
											<a href="#"><img src="img/ticket-network.png" alt></a>
										</div>
										<p class="ticket-price">$567</p>
									</li>
								</ul>
							</div>
						</div>
					</div><!-- /Buy list -->

					<!-- Buy detail -->
					<div class="buy-detail">
						<div class="section">
							<p class="event-nr">55</p>
							<div class="ticket-info">
								<p class="section-nr">Section 202</p>
								<p class="ticket-available">4 tickets available</p>
							</div>
						</div>
						<div class="buy-detail-price">
							<div class="price-box">
								<p class="price">$74 each</p>
								<p>with no additional fees or shipping</p>
							</div>
							<div class="group select-group">
								<div class="select-inner">
									<p class="select-info">Select your quantity</p>
									<select class="select-1">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
									</select>
								</div>
							</div>
						</div>
						<div class="buy-table">
							<ul>
								<li>
									<div class="column-1">
										<p>Section</p>
									</div>
									<div class="column-2">
										<p>Section 202</p>
									</div>
								</li>
								<li>
									<div class="column-1 optional">
										<p>Row</p>
									</div>
									<div class="column-2 column-select">
										<div class="group select-group">
											<div class="select-inner">
												<select class="select-1">
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="column-1 optional">
										<p>Seat</p>
									</div>
									<div class="column-2 column-select">
										<div class="group select-group">
											<div class="select-inner">
												<select class="select-1">
													<option>Seat 42 C</option>
													<option>Option 2</option>
													<option>Option 3</option>
													<option>Option 4</option>
												</select>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="column-1">
										<p>Tickets</p>
									</div>
									<div class="column-2">
										<p>2 tickets available</p>
									</div>
								</li>
								<li>
									<div class="column-1">
										<p>Deal score</p>
									</div>
									<div class="column-2">
										<p>68 - Good deal</p>
									</div>
								</li>
								<li>
									<div class="column-1">
										<p>Seller</p>
									</div>
									<div class="column-2 column-img">
										<img src="img/ticket-network.png" alt>
									</div>
								</li>
								<li>
									<div class="column-1">
										<p>Seller's note</p>
									</div>
									<div class="column-2">
										<p>3rd row, good seats</p>
									</div>
								</li>
							</ul>
							<p class="table-info">*Row and seat selection is optional</p>
						</div>
						<div class="group-submit">
							<button class="submit-btn back-btn">Back to all deals</button>
							<button class="submit-btn buy-btn">Buy</button>
						</div>

					</div><!-- /Buy detail -->

				</div><!-- /Tab buy -->

				<!-- Tab sell -->
				<div class="tab-sell">
					<h1 class="title">Follow the steps below</h1>
					<div class="slat-1 slat-sell">
						<h2>1. How many tickets are you selling?</h2>
						<div class="group select-group">
							<div class="select-inner">
								<p>Select your quantity</p>
								<select class="select-1">
									<option>4</option>
									<option>Option 1</option>
									<option>Option 2</option>
								</select>
							</div>
						</div>
						<p class="note">Note:<br>Tickets must be grouped together</p>
					</div>
					<div class="slat-2 slat-sell">
						<h2>2. Where are your tickets?</h2>
						<div class="ticket-location-box">
							<p class="ticket-class">A</p>
							<p>Click on the seating chart to select your selection</p>
							<p class="section-selected">Section 332 Selected<span></span></p>
						</div>
						<div class="ticket-location-box location-second">
							<p class="ticket-class">B</p>
							<p>Enter the row as it is printed on your tickets below</p>
							<p class="optional">Row and seat selection is optional</p>
							<div class="group select-group">
								<div class="group-inner">
									<input type="text" class="input-1">
								</div>
								<div class="select-inner">
									<select class="select-1">
										<option>seat selection</option>
										<option>Option 1</option>
										<option>Option 2</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="slat-3 slat-sell">
						<h2>3. Check all the following that apply:</h2>
						<ul>
							<li>
								<input type="checkbox" class="checkbox-1">
								<p>Etickets (email delivery)</p>
							</li>
							<li>
								<input type="checkbox" class="checkbox-1">
								<p>Obstructed view</p>
							</li>
							<li>
								<input type="checkbox" class="checkbox-1">
								<p>Aisle seats</p>
							</li>
							<li>
								<input type="checkbox" class="checkbox-1">
								<p>Partial view</p>
							</li>
							<li>
								<input type="checkbox" class="checkbox-1">
								<p>Pairs only</p>
							</li>
						</ul>
						<div class="group">
							<label class="label-1" for="additional_notes">Additional Notes (include seat numbers):</label>
							<input type="text" class="input-1" id="additional_notes" name="additional_notes">
						</div>
						<button class="submit-btn blue">View sell options</button>
					</div>
				</div><!-- /Tab sell -->
			</div>

			<div class="event-map">

				<div class="map-wrapper" data-method="eventsMap">
					<img class="mobile-location" src="img/stadium-1.jpg" alt>
					<img class="desktop-location" src="img/stadium-2.jpg" alt>
					<div class="map-pins">
						<div class="pin pin-1 green-pin" style="top: 88px; left: 273px;">
							<p class="pin-inner"><span>Section 202,</span> 2 listings from <span>$567</span></p>
						</div>
						<div class="pin pin-2 red-pin" style="top: 132px; left: 334px;">
							<p class="pin-inner"><span>Section 203,</span> 3 listings from <span>$367</span></p>
						</div>
						<div class="pin pin-3 yellow-pin" style="top: 49px; left: 373px;">
							<p class="pin-inner"><span>Section 204,</span> 4 listings from <span>$267</span></p>
						</div>
					</div>
				</div>
				<p class="event-location">Sun Life Stadium - Miami Gardens, FL on Wed Jun 25 at 8:00pm</p>
				<ul>
					<li class="green-price"><span></span>From $88</li>
					<li class="yellow-price"><span></span>From $678</li>
					<li class="red-price"><span></span>From $130</li>
					<li class="blue-price"><span></span>From $1750</li>
				</ul>
			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>