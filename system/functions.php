<?php
// Custom functions
function has_php_extension($file) {
	$ext = substr(strrchr($file, '.'), 1);
	if( trim($ext) == "php" ) {
		return true;
	} else {
		return false;
	}
}

function get_file($file) {
	$ext  = has_php_extension( $file ) ? '' : '.php';
	$file =  ABSPATH . $file . $ext;
	include( $file ); 
}


// return array
function get_uri_segments() {
	$uri = strtok( $_SERVER['REQUEST_URI'], '?' );
	$result = explode( "/", preg_replace("|/*(.+?)/*$|", "\\1", $uri ) );
	return $result;
}

function add_url_trailing_slash(){
	$uri = $_SERVER['REQUEST_URI'];
	if(substr($uri, -1) != "/"  ){
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.$uri."/");
		exit();
	}
}

function show_404_page(){
	header("HTTP/1.1 404 File not found"); 
	if( file_exists( ABSPATH . '404.php' ) ) {
		get_file( '404' );
	} else {
		exit('<h1>File not found</h1>');
	}
}

function current_url(){
	$protocol = 'http' . ( (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '' );
	return $protocol."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}	

function current_file() {
	$last_segment = array_pop(  get_uri_segments() );
	if( $last_segment == FOLDER_NAME ) {
		return 'home.php';	
	} else {

	}
	return array_pop(  get_uri_segments() ) . '.php';
}

function is_page($page) {
	$current_page = strtok( array_pop(  get_uri_segments() ), '?' );
	// strtok(array_pop( $uri_segments ), '?' );
	$page  = has_php_extension( $page ) ? str_replace('.php','', $page) : $page;

	// if is front page
	$index_page = array('home', 'frontpage', 'front', 'index');
	if( current_file() == 'home.php' && in_array($page, $index_page) ) {
		return true;
	} elseif ( $page == $current_page ) {
		return true;
	} else {
		return false;	
	}
}

function is_home(){
	return is_page( 'home' );
} 

function site_url(){
	global $project;
	$index_page = trim( $project['index_page'] ) != "" ? $project['index_page'] . "/" : "";
	$site_url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '')
                      .'://'.$_SERVER['HTTP_HOST'].str_replace('//','/',dirname($_SERVER['SCRIPT_NAME']).'/'); 
 	return $site_url;
}

function redirect($url) {
	header("Location: {$url}");
	exit;
}

function get_template_part($file){
	$ext  = has_php_extension( $file ) ? '' : '.php';
	$file =  ABSPATH . 'templates/' . $file . $ext;
	include( $file );
}
function get_header($file='header'){
	return get_template_part( $file );
}
function get_footer($file='footer'){
	return get_template_part( $file );
}

function get_sidebar($file="sidebar"){
	return get_template_part( $file );
}

function body_class() {
	$class_name = str_replace( '.php', '',current_file() );
	if( current_file() == '' ) {
		$class_name = 'home';
	}
	elseif( !file_exists( ABSPATH . current_file() ) ) {
		$class_name = 'page-404';
	} 
	else {
		$class_name = is_numeric($class_name[0]) ? "page-{$class_name}" : $class_name;
	}
	echo 'class="' . $class_name . '"';
}
include( 'file-helper.php' );
include( 'exporter.php' );