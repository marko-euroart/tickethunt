<?php

/* Export site in plain HTML */

global $site;
function xcopy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                xcopy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
} 

function prepare_links_for_export($html){
	$link_alone = '"' . site_url() . '"';
	$html_content = str_replace( $link_alone, '"#"', $html );
	return str_replace( site_url(), '', $html_content );
}
$last_segment = array_pop( get_uri_segments() );

if( $last_segment == 'export-to-html' && $site['enable_export_to_html'] == true  ) {
	// make folder
	$timestamp = time();
	$folder_name = FOLDER_NAME . "-html-" . $timestamp;

	if( !file_exists(ABSPATH . $folder_name ) )
		mkdir( ABSPATH . $folder_name );

	//copy into the folder
	$folders = array('css', 'img', 'js', 'less');
	foreach ($folders as $folder ) {
		xcopy( ABSPATH . $folder, ABSPATH . $folder_name . '/' . $folder );	
	}

	// get all php files
	$files = glob( ABSPATH . "*.php");
	foreach( $files as $file ) {
		if( $file == ABSPATH . 'index.php' || $file == ABSPATH . 'home.php' ) {
			$html_content = prepare_links_for_export( file_get_contents( site_url() ) );
			file_put_contents(ABSPATH . $folder_name . '/index.html', $html_content );
		} else {
			$basename = str_replace( '.php', '', basename($file) );
			$html_content = prepare_links_for_export( file_get_contents( site_url() . $basename .'/' ) );
			file_put_contents(ABSPATH . $folder_name . '/' . $basename .'.html', $html_content );
		}
	}
	redirect( site_url() . 'system/exported.php' );
}