<?php get_header('header'); ?>

<div id="promo" class="subpage" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
		</div>

	</div>
</div>

<div id="main" class="subpage">
	<div class="container">

		<div class="notifications-title">
			<div class="search-result">
				<h1>Notification</h1>
				<p>you have 84 Notifications</p>
			</div>
			<div class="sort-by">
				<ul class="sortby-notification">
					<li>By date</li>
					<li>Buy</li>
					<li>Sell</li>
					<li>Following</li>
					<li>Friends</li>
				</ul>
			</div>
		</div>

		<div class="sorted-results"></div>

	</div>
</div>

<?php get_footer(); ?>