<?php get_header('header'); ?>

<div id="promo" class="subpage" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
		</div>

	</div>
</div>

<div id="main" class="subpage">
	<div class="container">

		<div class="search-wrapper">
			<form action="#" class="search-form">
				<input type="text" class="search-bar" placeholder="Search by name, sport, venue..." />
				<input type="submit" class="search-btn" value="search">
			</form>
			<div class="contextual-search">
				<a href="#">Show all results...</a>
				<h1 class="title top-results">Top result</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title performers">Performers</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Nets</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Winter Festival</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Holiday International</h1>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title events">Events</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Celebrate Brooklyn - Perf. Arts Fest with Nick Cave & The Bad Seeds,
									Devendra Banhart, Nicole Atkins
								</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title venues">Venues</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Academy of Music</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>

		<div id="account-settings">
			<h1 class="title">Account settings</h1>
			<div class="tabs">
				<ul class="buttons">
					<li class="btn selected"><a href="#tab-1">User info</a></li>
					<li class="btn"><a href="#tab-2">Following</a></li>
					<li class="btn"><a href="#tab-3">My tickets</a></li>
				</ul>
			</div>
			<div id="tab-1">
				<div class="user-info">
					<form action="#">
						<div class="section basic-info">
							<h1>Basic information</h1>
							<div class="group">
								<input class="user" type="text" placeholder="Matija">
							</div>
							<div class="group">
								<input class="user" type="text" placeholder="Lović">
							</div>
							<div class="group">
								<input class="email" type="text" placeholder="Email">
							</div>
							<div class="group">
								<input type="button" class="change-pass" value="Change password">
							</div>
						</div>
						<div class="section user-location">
							<h1>Location</h1>
							<div class="group">
								<input class="location" type="text" placeholder="Duga Resa">
							</div>
						</div>
						<div class="section profile-pic">
							<h1>Profile Picture</h1>
							<div class="user-img"></div>
							<label for="upload-file">Upload your profile picture</label>
							<div class="fupload-wrapper">
								<span class="fup-name"></span>
								<input id="upload-file" type="file" class="upload-file" name="upload">
							</div>
							<p class="instruction">max. image size is 45x45 px with maximum file size limit of 2mb</p>
						</div>
						<div class="section mail">
							<h1>Email</h1>
							<div class="group">
								<input type="checkbox" name="checkbox" class="remember-me" id="remember">
								<label for="remember">Event Alerts</label>
								<p>Tell me about cool events in my area</p>
							</div>
							<div class="group">
								<input type="checkbox" name="checkbox" class="remember-me" id="remember">
								<label for="remember">Ticket Alerts</label>
								<p>Tell me about really great deals on tickets that might not last very long</p>
							</div>
							<div class="group">
								<input type="checkbox" name="checkbox" class="remember-me" id="remember">
								<label for="remember">Product News</label>
								<p>Send me information about new products and features on TicketHunt</p>
							</div>
							<div class="group break">
								<input type="checkbox" name="checkbox" class="remember-me" id="remember">
								<label for="remember">Ticket hunt spotlight</label>
								<p>Send me the TicketHunt Spotlight, which highlights a new up-and-coming
									artist every week.
								</p>
							</div>
							<a href="#" class="unsubscribe">Unsubscribe from all Ticket Hunt emails</a>
							<input type="submit" value="Update your account">
						</div>
					</form>
				</div>
			</div>
	
			<div id="tab-2">
				<div class="following">
					<div class="hunter-wrapper">
						<div class="description">	
							<h1>Your live event hunter</h1>
							<h2>Track events and artists or find out about ticket deals you'll love</h2>
						</div>
						<form action="#">
							<div class="group">
								<input type="search" placeholder="Enter an artist or a band">
								<input type="submit" class="search-btn" value="search">
							</div>
						</form>
						<ul class="options">
							<li><a href="#">
								<div class="flip">
									<div class="inner">
										<div class="sticker front">
											<img src="<?php echo site_url() ?>img/microphone-icon2.png" alt="*">
										</div>
										<div class="sticker back">
											<img src="<?php echo site_url() ?>img/microphone-icon2-hover.png" alt="*">
										</div>
									</div>
									<div class="sticker-name">
										<p>Artists</p>
									</div>
								</div>
							</a></li>
							<li><a href="#">
								<div class="flip">
									<div class="inner">
										<div class="sticker front">
											<img src="<?php echo site_url() ?>img/calendar-icon2.png" alt="*">
										</div>
										<div class="sticker back">
											<img src="<?php echo site_url() ?>img/calendar-icon2-hover.png" alt="*">
										</div>
									</div>
									<div class="sticker-name">
										<p>Events</p>
									</div>
								</div>
							</a></li>
							<li><a href="#">
								<div class="flip">
									<div class="inner">
										<div class="sticker front">
											<img src="<?php echo site_url() ?>img/venue-icon.png" alt="*">
										</div>
										<div class="sticker back">
											<img src="<?php echo site_url() ?>img/venue-icon-hover.png" alt="*">
										</div>
									</div>
									<div class="sticker-name">
										<p>Venue</p>
									</div>
								</div>
							</a></li>
						</ul>
					</div>

					<div class="results">
						<div class="box">
							<a href="#"><img class="thumb" src="<?php echo site_url() ?>img/small-thumb.jpg" alt="*"></a>
							<div class="box-content">
								<h1><a href="#">U2</a></h1>
								<p>Chicago, on 30 Jul 2014 at 20:00</p>
							</div>
							<span class="close">x</span>
						</div>
						<div class="box">
							<a href="#"><img class="thumb" src="<?php echo site_url() ?>img/small-thumb2.jpg" alt="*"></a>
							<div class="box-content">
								<h1><a href="#">George Strait with Lee Ann Womack</a></h1>
								<p>Chicago, on 30 Jul 2014 at 20:00</p>
							</div>
							<span class="close">x</span>
						</div>
						<div class="box">
							<a href="#"><img class="thumb" src="<?php echo site_url() ?>img/small-thumb3.jpg" alt="*"></a>
							<div class="box-content">
								<h1><a href="#">Beyonce with Jay-Z</a></h1>
								<p>Chicago, on 30 Jul 2014 at 20:00</p>
							</div>
							<span class="close">x</span>
						</div>
						<div class="box">
							<a href="#"><img class="thumb" src="<?php echo site_url() ?>img/small-thumb4.jpg" alt="*"></a>
							<div class="box-content">
								<h1><a href="#">Eminem & Rihana</a></h1>
								<p>Chicago, on 30 Jul 2014 at 20:00</p>
							</div>
							<span class="close">x</span>
						</div>
						<!-- <div class="results-empty">
							<p>YOU'RE NOT FOLLOWING ANYTHING</p>
							<p>Please add artist/event/venue to follow</p>
						</div> -->
					</div>
				</div>
			</div>

			<div id="tab-3">
				<div class="event-map">
					<h1>Event venue</h1>
					<img src="img/stadium-1.jpg">
					<p>Sun Life Stadium — Miami Gardens, FL on Wed Jun 25</p>
				</div>
				<div class="ticket-column" data-method="ticketTabs">
					<p class="ticket-btn sale active" data-tab="1">For sale</p>
					<p class="ticket-btn bought" data-tab="2">Bought tickets</p>

					<div class="ticket-tabs">
						<ul class="ticket-list ticket-tab-1">
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>One republic</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li class="active"><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>One republic</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li class="active"><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>One republic</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li class="active"><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>One republic</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li class="active"><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
						</ul>

						<ul class="ticket-list ticket-tab-2">
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>Lady gaga with rolling stones</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li class="active"><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>Lady gaga with rolling stones</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li class="active"><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>Lady gaga with rolling stones</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li class="active"><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="ticket-info">
									<div class="event-date">
										<p class="date">Jul 11</p>
										<p>Saturday</p>
										<p class="time">8:00 PM</p>
									</div>
									<div class="ticket-detail">
										<h1>Lady gaga with rolling stones</h1>
										<p>MGM Grand Garden Arena - Las Vegas, NV</p>
										<p class="ticket-price">Price <span>$89,00</span><span class="divider"></span> Quantity <span>2</span></p>
									</div>
									<ul class="ticket-option">
										<li><a href="#">Cancel</a></li>
										<li><a href="#">Complain</a></li>
										<li><a href="#">Sell</a></li>
									</ul>
									<div class="ticket-status">
										<ul>
											<li><span>Requested</span></li>
											<li><span>Verified</span></li>
											<li><span>Paid</span></li>
											<li><span>Delivered</span></li>
											<li class="active"><span>Closed</span></li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

		</div>


	</div>
</div>

<?php get_footer(); ?>