<?php get_header(); ?>
<!-- Promo -->
<div id="promo" data-method="homeSlider">
	<div class="container">
		
		<div class="slider-imgs">
			<span><img class="slide1 current-showing" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
			<span><img class="slide2" src="<?php echo site_url() ?>img/slider-img-2.jpg" alt="*"></span>
			<span><img class="slide3" src="<?php echo site_url() ?>img/slider-img-1.jpg" alt="*"></span>
			<span><img class="slide4" src="<?php echo site_url() ?>img/slider-img-2.jpg" alt="*"></span>
		</div>

		<div class="slider">
			<div class="slider-wrapper" data-method="sliderArrows">
				<div class="slide" data-slide="slide1">
					<div class="slide-content-wrapper">
						<article>
							<h1>Daft punk live</h1>
							<p>Rose Bowl - pasadena, CA on sat Aug 2 at 8:00pm</p>
							<i class="prev"></i>
							<i class="next" data-slide="2"></i>
						</article>
					</div>
				</div>
				<div class="slide" data-slide="slide2">
					<div class="slide-content-wrapper">
						<article>
							<h1>Daft punk live</h1>
							<p>Rose Bowl - pasadena, CA on sat Aug 2 at 8:00pm</p>
							<i class="prev"></i>
							<i class="next"></i>
						</article>
					</div>
				</div>
				<div class="slide" data-slide="slide3">
					<div class="slide-content-wrapper">
						<article>
							<h1>Daft punk live</h1>
							<p>Rose Bowl - pasadena, CA on sat Aug 2 at 8:00pm</p>
							<i class="prev"></i>
							<i class="next"></i>
						</article>
					</div>
				</div>
				<div class="slide" data-slide="slide4">
					<div class="slide-content-wrapper">
						<article>
							<h1>Daft punk live</h1>
							<p>Rose Bowl - pasadena, CA on sat Aug 2 at 8:00pm</p>
							<i class="prev"></i>
							<i class="next"></i>
						</article>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- /Promo -->

<!-- Main -->
<div id="main">
	<div class="container">

		<div class="search-wrapper">
			<form action="#" class="search-form">
				<input type="text" class="search-bar" placeholder="Search by name, sport, venue..." />
				<input type="submit" class="search-btn" value="search">
			</form>
			<div class="contextual-search">
				<a href="#">Show all results...</a>
				<h1 class="title top-results">Top result</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title performers">Performers</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Nets</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Winter Festival</h1>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Hoops Holiday International</h1>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title events">Events</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Walking with Dinosaurs - Brooklyn</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
					<li>
						<a href="#">
							<div>
								<h1>Celebrate Brooklyn - Perf. Arts Fest with Nick Cave & The Bad Seeds,
									Devendra Banhart, Nicole Atkins
								</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
				<h1 class="title venues">Venues</h1>
				<ul>
					<li>
						<a href="#">
							<div>
								<h1>Brooklyn Academy of Music</h1>
								<p>Barclay's Center on 26'th Jul 2014, 21:00</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
			<div class="title content">
				<p>
					Find sports and concert tickets. For instance, try
					<span>Justin Timberlake</span>, <span>Brooklyn Nets</span>, <span>Madison Square Garden</span>,
					 or <span>New York.</span>
				</p>
			</div>
		</div>

		<div class="events">
			<h1 class="title"><span>Currently</span><span class="event-number">13.400</span> events</h1>
			<div class="features" data-method="featureAnim">
				<div class="feature odd">
					<div class="thumb">
						<div class="thumb-wrapper">
							<a href="#"><img class="black-white" src="<?php echo site_url() ?>img/globe-sticker-bw.png" alt="*"></a>
							<a href="#"><img class="colored" src="<?php echo site_url() ?>img/globe-sticker.png" alt="*"></a>				</div>
						</div>
					<article>
						<a href="#"><h1 class="blue">Seek and find</h1></a>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
							Nam urna metus, bibendum a tristique vitae, consectetur at nisl. 
							Sed vulputate lectus eu neque feugiat, eu ultricies purus vehicula. 
						</p>
					</article>
				</div>

				<div class="feature even">
					<div class="thumb">
						<div class="thumb-wrapper">
							<a href="#"><img class="black-white" src="<?php echo site_url() ?>img/people-sticker-bw.png" alt="*"></a>
							<a href="#"><img class="colored" src="<?php echo site_url() ?>img/people-sticker.png" alt="*"></a>
						</div>
					</div>
					<article>
						<a href="#"><h1 class="pink">Attend what you really enjoy</h1></a>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
							Nam urna metus, bibendum a tristique vitae, consectetur at nisl. 
							Sed vulputate lectus eu neque feugiat, eu ultricies purus vehicula. 
						</p>
					</article>
				</div>

				<div class="feature odd">
					<div class="thumb">
						<div class="thumb-wrapper">
							<a href="#"><img class="black-white" src="<?php echo site_url() ?>img/ticket-sticker-bw.png" alt="*"></a>
							<a href="#"><img class="colored" src="<?php echo site_url() ?>img/ticket-sticker.png" alt="*"></a>
						</div>
					</div>
					<article>
						<a href="#"><h1 class="pink">Know what you pay for</h1></a>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
							Nam urna metus, bibendum a tristique vitae, consectetur at nisl. 
							Sed vulputate lectus eu neque feugiat, eu ultricies purus vehicula. 
						</p>
					</article>
				</div>

				<div class="feature even">
					<div class="thumb">
						<div class="thumb-wrapper">
							<a href="#"><img class="black-white" src="<?php echo site_url() ?>img/pc-sticker-bw.png" alt="*"></a>
							<a href="#"><img class="colored" src="<?php echo site_url() ?>img/pc-sticker.png" alt="*"></a>						
						</div>
					</div>
					<article>
						<a href="#"><h1 class="blue">Sell what you don't need</h1></a>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
							Nam urna metus, bibendum a tristique vitae, consectetur at nisl. 
							Sed vulputate lectus eu neque feugiat, eu ultricies purus vehicula. 
						</p>
					</article>
				</div>
			</div>
		</div>
		
	</div>

	<div class="popular-events" data-method="iconsPopular">
		<div class="container">
			<h1 class="title">Popular events</h1>
			<form action="#">
				<label for="category">Sort by:</label>
					<select id="category" class="category">
						<option value="all">All</option>
						<option value="option-1">option-1</option>
						<option value="option-2">option-2</option>
						<option value="option-3">option-3</option>
					</select>
			</form>
			<div class="box-wrapper">
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-1.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-2.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-5.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-4.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-5.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-6.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-7.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
				<div class="box">
					<div class="thumb">
						<span class="category">Concerts</span>
						<a href="#">
							<img src="<?php echo site_url() ?>img/sample-1.jpg" alt="*">
							<p><span>View details</span></p>
						</a>
					</div>
					<div class="box-content">
						<a href="#"><h1>U2 - Live in Amsterdam Arena</h1></a>
						<h2>Chicago, on 30 jul 2014. at 20:00</h2>
						<a href="#" class="favorite"></a>
						<a href="#" class="add"></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="popular-searches">
		<div class="container">
			<h1 class="title">Popular searches</h1>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
			<div class="search-list-wrapper">
				<h1>Nfl tickets</h1>
				<ul>
					<li>Sesttle Seahawks</li>
					<li>San Francisko 49ers</li>
					<li>New England Patriots</li>
					<li>Dallas Cowboys</li>
				</ul>
				<a class="more" href="#">More</a>
			</div>
		</div>
	</div>
</div>
<!-- /Main -->
<?php get_footer(); ?>