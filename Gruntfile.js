module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

  	/*----------------------------------------------------------------------------- LESS */
  	less: {
	    dist: {
	      files: [
	        {
	          expand: true,     	// Enable dynamic expansion.
	          cwd: 'less/',      	// Src matches are relative to this path.
	          src: ['**/*.less'], 	// Actual pattern(s) to match.
	          dest: 'css/',   		// Destination path prefix.
	          ext: '.css',   		// Dest filepaths will have this extension.
	        },
	      ],
	    },
	},

	/*----------------------------------------------------------------------------- IMAGE MINIFICATION */
	// not yet in use
	imagemin: {
		dynamic: {                         // Another target
	      files: [{
	        expand: true,                  // Enable dynamic expansion
	        cwd: 'img/',                   // Src matches are relative to this path
	        src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
	        dest: 'images/'                // Destination path prefix
	      }]
	    }
	},

	/*----------------------------------------------------------------------------- JS HINT */
	jshint: {
    	all: ['Gruntfile.js', 'js/**/*.js']
  	},

  	/*----------------------------------------------------------------------------- CONTACT JAVASCRIPT */
  	concat: {
		options: {
		  separator: ''
		},
		dist: {
		  src: ['js-dev/ea-init.js', 'js-dev/plugins/**/*.js', 'js-dev/global.js', 'js-dev/methods/**/*.js'],
		  dest: 'js/script.js'
		}
	},

	/*----------------------------------------------------------------------------- UGLIFY */
	uglify: {
		my_target: {
		  files: {
		    'js/script.min.js': ['js/script.js']
		  }
		}
	},

  	/*----------------------------------------------------------------------------- WATCH */
	watch: {
      files: ['less/*.less', 'js-dev/**/*.js'],
      tasks: ['less', 'concat'],
      options: {
      	livereload: false
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // grunt.loadNpmTasks('grunt-contrib-jshint');

  // Default task(s).
  grunt.registerTask( 'default', ['less', 'concat']);
  grunt.registerTask( 'minify-images', ['imagemin:dynamic']);
  grunt.registerTask( 'uglify-js', ['uglify']);
};