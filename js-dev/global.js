$(document).ready(function(){

function centerSlides(){
promo =  $('.slider-imgs');
slideImg = $('.slider-imgs img')
slideSpan = $('.slider-imgs span')
var promoWidth = promo.width();
var imgWidth = slideImg.width();
var spanMargin = (imgWidth - promoWidth) / 2;
    if (spanMargin >= 0){
        slideImg.css({ 'width': 'auto' });
        slideSpan.css({ 'margin-left': -spanMargin });
    } else {
        slideImg.css({ 'width': '100%' });
    }
}

//stellar paralax
	$(window).stellar();

//fancy select
	$('select.category, .select-1').fancySelect();

//scrollbar
 	$('.scrollbar-outer').scrollbar();

//placeholder
	$('input, textarea').placeholder();

//user-info btns 
	$('.options li').click(function(){
		$(this).toggleClass('active');
		event.preventDefault();
	});

//pop-up trigers
	$('.login').click(function(){
		$('#modal.login-trigger').fadeIn();
	});
	$('.close').click(function(){
		$('#modal.login-trigger').fadeOut();
	});

	$('.signup').click(function(){
		$('#modal.registration-trigger').fadeIn();
	});
	$('.close').click(function(){
		$('#modal.registration-trigger').fadeOut();
		$('#modal.change-pass-trigger').fadeOut();
	});
	$('.forgot-pass').click(function(event){
		event.preventDefault();
		$('#modal.restore-pass-trigger').fadeIn();
		$('#modal.login-trigger .modal-box').fadeOut();
	});
	$('.close-restore').click(function(){
		$('#modal.restore-pass-trigger').fadeOut();
		$('#modal.login-trigger .modal-box').fadeIn();
	});
	$('.change-pass').click(function(event){
		event.preventDefault();
		$('#modal.change-pass-trigger').fadeIn();
	});
	$('.location').click(function(){
		$('.results.second-option').toggleClass('active');
		$(this).toggleClass('active');
		$(this).siblings().removeClass('active').children().removeClass('active');

	});
	$('.time').click(function(){
		$('.results.third-option').toggleClass('active');
		$(this).toggleClass('active');
		$(this).siblings().removeClass('active').children().removeClass('active');
	});


//account settings trigers
	$('.btn').click(function(){
		$(this).toggleClass('selected');
		$(this).siblings().removeClass('selected');
		event.preventDefault();
	});
	$('#account-settings').tabs({ active: 0 });	
	$('#artists-tabs').tabs({ active: 0 });	

//custom file upload 

	// $(function(){
	// 	$('input[type=file]').change(function(){
	// 		$(this).siblings('span.fup-name').text(this.value.replace(/^.*[\\\/]/, ''));
	// 	});
	// });

	// jQuery.noConflict();
	// (function($){
	// window.onload=function(){
	//     $("#tS2").thumbnailScroller({
	//         scrollerType:"hoverPrecise",
	//         scrollerOrientation:"horizontal",
	//         scrollSpeed:2,
	//         scrollEasing:"easeOutCirc",
	//         scrollEasingAmount:600,
	//         acceleration:4,
	//         scrollSpeed:800,
	//         noScrollCenterSpace:10,
	//         autoScrolling:0,
	//         autoScrollingSpeed:2000,
	//         autoScrollingEasing:"easeInOutQuad",
	//         autoScrollingDelay:500
	//     });
	// }
		
// Slider arrows
EA.Methods.sliderArrows = function($this){
	var owlSlider = $(".slider-wrapper").data('owlCarousel');
	$('.next').click(function(){
		owlSlider.goTo($(this).data('slide'));
	});
}

//slider
	var owl = $(".slider-wrapper");

	owl.owlCarousel({
		navigation : false,
		singleItem : true,
		pagination : true,
		navigation : true,
		// autoPlay: true,
		slideSpeed: 400,
		transitionStyle : "fadeUp",
		addClassActive: true,
		
		afterMove : function (){
			var slideId = $('.owl-item.active .slide').data('slide');
			
			setTimeout(function(){
				$('.slider-imgs img').removeClass('current-showing');
				$('.slider-imgs .'+slideId).addClass('current-showing');
			}, 100 );
		}
	});


//slider img center

	setTimeout(function(){
		centerSlides();
	}, 1 );

    centerSlides();
        $(window).resize(function() {
           centerSlides();
        });
//Placeholder
	$('input').placeholder();

});

//checkbox
	$('input:checkbox').screwDefaultButtons({ 
		image: "url(img/check.png)",
		width:	 20,
		height:	 20
	});

//onfocus apperaence
	$('.search-bar').focus(function(){
		$('head meta[name=viewport]').remove();
		$('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">');
		$('.contextual-search').addClass('active')
	});

	$('.search-bar').focusout(function(){
		$('head meta[name=viewport]').remove();
		$('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
		$('.contextual-search').removeClass('active');
	});

//calendar
	
$('.ticket-sale').click(function(){
	var popupId = $(this).data('id');
	var pos = $(this).position();
	console.log( pos );
	$('.calendar-popup.box-'+ popupId).addClass('active').css({"top": pos.top, "left" : pos.left+155}).siblings().removeClass('active');

});

// Features animation6
EA.Methods.featureAnim = function($this){
	$('.feature').mouseover(function(e){
		$(this).siblings().addClass('inactive');
	});
	$('.feature').mouseout(function(e){
		$(this).siblings().removeClass('inactive');
	});
}

// Popular events icons-animation
EA.Methods.iconsPopular = function($this){
	$('.favorite').click(function(e){
		event.preventDefault();
		$(this).toggleClass('active');
	});
	$('.add').click(function(e){
		event.preventDefault();
		$(this).toggleClass('active');
	});
}

EA.Methods.mobileNav = function($this){
	$('.menu-btn').click(function(e){
		if($('body').hasClass('mob-nav-open')){
			$('body').removeClass('mob-nav-open');
			setTimeout(function(){
				$('.mob-nav-2.active').removeClass('active');
				$('.mob-nav-3.active').removeClass('active');
				$('.profile-menu-mob').removeClass('active');
			}, 500);
		} else {
			$('body').addClass('mob-nav-open');
		}
	});

	$('.menu-step-1 .step-1 span').click(function(e){
		// event.preventDefault();
		$(this).parent().find('.mob-nav-2').addClass('active');
	});

	$('.menu-step-2 li span').click(function(e){
		// event.preventDefault();
		$(this).parent().find('.mob-nav-3').addClass('active');
	});

	$('.back-1, .back-2').click(function(e){
		$(this).parent().removeClass('active');
	});	
}

EA.Methods.profileMenu = function($this){
	$('.profile-inner').click(function(event){
		$(this).parent().toggleClass('active');
	});
}

EA.Methods.ticketTabs = function($this){
	var button = $('.ticket-btn');

	button.click(function(event){
		$(this).addClass('active').siblings().removeClass('active');
		var tabId = $(this).data('tab');
		$('.ticket-list.ticket-tab-'+tabId).show().siblings().hide();
	});
}

EA.Methods.mobileProfile = function($this){
	$('.profile-box-mobile').click(function(e){
		$('.profile-menu-mob').addClass('active');
	});
	$('.back-profile').click(function(e){
		$(this).parent().removeClass('active');
	});
}

EA.Methods.eventSearch = function($this){
	$('#events .title').click(function(e){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).parent().find('.search-event').fadeOut(400);
		} else {
			$(this).addClass('active');
			$(this).parent().find('.search-event').fadeIn(400);
		}
	});
}
EA.Methods.calendarPopups = function($this){
	$('.day-content ul li span').click(function(e){
		if($(this).parent().hasClass('active')){
			$(this).parent().removeClass('active');
		} else {
			$(this).parent().addClass('active').siblings().removeClass('active');
		}
	});
	$('.close-box').click(function(e){
		$(this).closest('.ticket-box').removeClass('active');
	});
	$('.friends-popup-trigger').click(function(e){
		$(this).parent().find('.friends-box').addClass('active');
	});
	$('.close-2').click(function(e){
		$(this).parent().removeClass('active');
	});
}

EA.Methods.stickyCalendar = function($this){
	if($(window).width() > 1161){
		$(".calendar-top").addClass('active');
    	$(".calendar-top.active").sticky({topSpacing:0});
	}

	$(window).resize(function(){
		if($(window).width() > 1161){
			$(".calendar-top").addClass('active');
    		$(".calendar-top.active").sticky({topSpacing:0});
		}
		else {
			$(".calendar-top").removeClass('active');
		}
	});
}

EA.Methods.calendarToppopups = function($this){
	$('.calendar-icons .inner').click(function(e){
		$('.icons-popup').removeClass('active');
		$(this).parent().find('.icons-popup').addClass('active');
	});
	$('.close-list').click(function(e){
		if($(this).closest('ul').find('li.active').length > 1){
			$(this).closest('li').removeClass('active').slideUp(500);
			console.log($(this).closest('ul').find('li.active').length);
		} else {
			$(this).closest('li').removeClass('active').fadeOut(0);
			$(this).closest('.icons-popup').removeClass('active');
		}
	});
	$('.icons-popup .close-popup').click(function(e){
		$(this).parent().removeClass('active');
	});
}

EA.Methods.calendarAnimations = function($this){
	$('.tick-sale').click(function(e){
		$(this).effect( 'pulsate', {times:3}, 500 );
	});

	var positionSel = 0;
	$('.calendar-icons .inner').click(function(e) {
		$(this).find('.cal-icon').animate({backgroundPosition: 'center -=42px'});
	});
}

EA.Methods.uiSlider = function($this){
	$( "#slider-range" ).slider({
		range: true,
		min: 5,
		max: 1000,
		values: [ 275, 600 ],
		slide: function( event, ui ) {
			$( "#min-amount span" ).text(ui.values[ 0 ]);
			$( "#max-amount span" ).text(ui.values[ 1 ]);
		}
	});

	$('#min-amount span').text($('#slider-range').slider('values', 0));
	$('#max-amount span').text($('#slider-range').slider('values', 1));

	$( "#slider-range" )
}

EA.Methods.calendarSlider = function($this){
	if ($(window).width() > 1140){
		$('.newsticker').each(function(){
			// console.log($(this).height());
			if($(this).height() > 56){
				$(this).newsTicker({
					row_height: 15,
					max_rows: 3,
					autostart: 0,
				    prevButton: $(this).parent().addClass('active').find('.top-btn'),
				    nextButton: $(this).parent().addClass('active').find('.down-btn'),
				});
			}
		});
	}
	$(window).resize(function() {
		console.log($(window).width());
		if ($(window).width() < 1160){
			$('.newsticker').css({'height': '', 'overflow': ''});
		} else {
			$('.newsticker').each(function(){
				if($(this).height() > 56){
					$(this).css({'height': '48', 'overflow': 'hidden'});
					$(this).newsTicker({
						row_height: 15,
						max_rows: 3,
						autostart: 0,
					    prevButton: $(this).parent().addClass('active').find('.top-btn'),
					    nextButton: $(this).parent().addClass('active').find('.down-btn'),
					});
				}
			});
		}
	});
}

EA.Methods.eventsMap = function($this){
	$('.pin').click(function(e){
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('html').click(function() {
		$('.pin').removeClass('active');
	});

	$('.pin').click(function(event){
	    event.stopPropagation();
	});
}

EA.Methods.eventsDetail = function($this){
	$('.events-list li').click(function(e){
		$('.buy-list').fadeOut(500);
		setTimeout(function(e){
			$('.buy-detail').fadeIn(500);
		}, 500);
	});
	$('.buy-detail .back-btn').click(function(e){
		$('.buy-detail').fadeOut(500);
		setTimeout(function(e){
			$('.buy-list').fadeIn(500);
		}, 500);
	});
}

EA.Methods.eventsTabs = function($this){
	// $('.event-tabs li').click(function(e){
	// 	$(this).addClass('active').siblings().removeClass('active');
	// 	console.log($(this).data('tab'));
	// });
	$('.buy-trigger').click(function(e){
		if($(this).hasClass('active')){

		} else {
			$(this).addClass('active').siblings().removeClass('active');
			$('.tab-sell').fadeOut(500);
			setTimeout(function(e){
				$('.tab-buy').fadeIn(500);
			}, 500);
		}
	});
	$('.sell-trigger').click(function(e){
		if($(this).hasClass('active')){

		} else {
			$(this).addClass('active').siblings().removeClass('active');
			$('.tab-buy').fadeOut(500);
			setTimeout(function(e){
				$('.tab-sell').fadeIn(500);
			}, 500);
		}
	});
}



$(document).ready(function(){
     var oldScrollTop = 0;
     var section1 = $('#promo');
      section1.css({ 'top': '0' });
    // console.log(section1.length);
    $(window).scroll(function(){
        var topOffset = $(document).scrollTop();
        var delta = topOffset - oldScrollTop;
        var deltaAbs = Math.abs( delta );
 
         //section1.css({'-webkit-transform': 'translate3d(0,0,0)'});
        if( delta > 0 ) {
                
            //section1.css({ '-webkit-transform': 'translate3d(0px, -'+( deltaAbs / 2 )+'px, 0px)' });
             section1.css({ 'top': '+='+(deltaAbs / 3)+'px' });
        } else {
            //section1.css({ '-webkit-transform': 'translate3d(0px, -'+(deltaAbs / 2)+'px, 0px)' });
            section1.css({ 'top': '-='+(deltaAbs / 3)+'px' });
        }
        oldScrollTop = topOffset;
    });
});
